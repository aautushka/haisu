[![Build Status](https://travis-ci.org/aautushka/haisu.svg?branch=master)](https://travis-ci.org/aautushka/haisu)

## Synopsis

HAISU - it is a C++ junk-yard. Every developer needs one. I commit all sorts of tiny useful (hopefully) classes here. Could this grow into something bigger? Nobody knows.

I habitually use Debian, Arch Linux and MacOS for my coding. HAISU would probably compile on these platforms at all times.

## License

Distributed under the MIT license. Copyright (c) 2017, Anton Autushka
